# PDK Puppet Exercise
This exercise will help you understand the PDK commands and how to setup class parameters and the layout of 
classes for software. This pattern is widely used throughout the puppet community and you will find similar patterns
in many other modules.  The majority of this exercise should be done using PDK.  For help see references at the bottom.

Time involved 20-90 minutes

## Basic setup
For this exercise you will need to complete the following:

1. Follow the steps outlined in the workstation setup
2. Create a module with the given requirements:

### Module requirements
1. Use vscode
1. A new module with namespace: name-foobar
2. Create the following classes in this new module
   - service
   - package
   - config
   - foobar (init.pp)

5. Create the following class parameters for the foobar class.  Each class parameter should have a datatype. 

   - manage_service
   - manage_package
   - manage_config
   - package_ensure
   - config_path
   - enable_service
   - service_ensure
   - package_name
   - service_name

  Note: If the class parameter name looks like a question the datatype should be a Boolean.

### Create the resources

  1. In the service class create a resource that manages the service called `$foobar::service_name`.
  2. Set the ensure parameter to `$foobar::service_ensure` and set the enable parameter `$foobar::enable_service`
  3. In the package class create a package resource with the ensure value set to `$foobar::package_ensure`
  4. In the config class create a file resource with the title `$foobar::config_path`

### Setup init.pp class

  5.  In the init.pp include all the classes you just created example: `include foobar::package`

  6.  Add the following after the include statements to order the classes

```ruby
  Class[Foobar::Package]
  -> Class[Foobar::Config]
  -> Class[Foobar::Service]

```

### Management variables
Since this module allows the user to individually manage the service, package and configuration you will need
to add a conditional statement around each resource (package, service, config resources). 
The default package and service name should be named `foobar`, however the user should be able to change this with hiera data at any time.

## Optional step - module-data
If you finished quickly you can add support for multiple operating systems and versions.  To do this you will need to setup hiera module data.  Luckily, pdk created most of this for you.

As a reference you can use the puppet-example module located in `<gitlab_server>/<puppet group>/puppet-example`.  These generic files should be enough
to get you started.

  - data/os/RedHat/7.yaml
  - data/os/RedHat/8.yaml

Each file should override the package name and service name.  Redhat 8 changed the foobar service and package name to fooeybar so you would need to support
this change for that OS version.

## Required step
At any point during this exercise you will need to create a repository for your new module in gitlab.  This should be in your own user space and not in the group.

Using the git commands below gitlab will automatically create the project for you so there is no need to manually create the project.

The steps I have below use ssh as the protocol which assumes you have

1. Created a ssh key using the git bash command `ssh-keygen -b 4096`
2. Added your ssh key to your gitlab profile settings

To create a gitlab repository you should run the following after making your module a git repository.

  ```
    git init
    git checkout -b main
    git add .
    git commit -m "init commit"
    git push git@gitlab-server:/<username>/puppet-foobar.git main
  ```

## Last step
You should always run `pdk validate` and `pdk test unit`.  
To automatically fix lint errors you can run `pdk validate -a`.  

Running the unit tests will help identify syntax errors.

Once you have completed coding the module, remember to commit and push your code.



## References
Below are some references to helpful documentation.

https://puppet.com/docs/pdk/2.x/pdk_creating_modules.html

https://puppet.com/docs/puppet/6/lang_data_type.html

https://www.git-tower.com/learn/git/faq/git-create-repository/

https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account

