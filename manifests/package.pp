# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include foobarr::package
class foobarr::package {
  if $foobarr::manage_package {
    package{$foobarr::package_name:
      ensure => $foobarr::package_ensure
    }
  }
}
