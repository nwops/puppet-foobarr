# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include foobarr::service
class foobarr::service {
  if $foobarr::manage_service {
    service{$foobarr::service_name:
      ensure => $foobarr::service_ensure,
      enable => $foobarr::enable_service
    }
  }

}
