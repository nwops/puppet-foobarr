class foobarr(
  Boolean $manage_service = true,
  Boolean $manage_package = true,
  Boolean $manage_config  = true,
  Enum[present,absent] $package_ensure = 'present',
  Enum[stopped, running] $service_ensure = 'running',
  Enum[present,absent] $config_ensure = 'present',
  String $config_path = '/etc/foobarr.conf',
  Boolean $enable_service = true,
  String $package_name = 'foobarr',
  String $service_name = 'foobarr'
) {

  include foobarr::config
  include foobarr::package
  include foobarr::service

  Class['foobarr::package']
  -> Class['foobarr::config']
  -> Class['foobarr::service']
}
