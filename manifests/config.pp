# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include foobarr::config
class foobarr::config {
  if $foobarr::manage_config {
    file{$foobarr::config_path:
      ensure  => $foobarr::config_ensure,
      content => epp('foobarr/config.conf')
    }
  }
}
